package view;

import browser.NanoBrowser;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

import java.net.URL;
import java.util.*;
import java.lang.Exception;

public class NanoBrowserView {
    public static final String BLANK = " ";
    private WebView myPage;
    // navigation
    private TextField myURLDisplay;
    // information area
    private Label myStatus;
    private NanoBrowser myBrowser;
    private Optional<String> myHomePage;
    private TextInputDialog homePage;
    private boolean startBrowser = true;
    private ChoiceBox<String> myFavorites;
    private ChoiceBox<String> myTopSites;
    private String selectedFavorite;
    private String selectedTopSite;
    private HashMap<String, Integer> myVisitedSites = new HashMap<>();
    private String myHomePageLink;
    private SplitMenuButton myRecentSites;
    private HashMap<String, URL> myFavoriteSites = new HashMap<>();
    private ArrayList<String> myFavoriteNames = new ArrayList<>();

    /**
     * Returns scene for the browser, so it can be added to stage.
     */
    public NanoBrowserView(NanoBrowser browser){
        myBrowser = browser;
    }

    public Scene makeScene (int width, int height) {
        BorderPane root = new BorderPane();
        // must be first since other panels may refer to page
        root.setCenter(makePageDisplay());
        root.setTop(makeTopPanel());
        root.setRight(makeRightPanel());
        root.setBottom(makeInformationPanel());
        // create scene to hold UI
        return new Scene(root, width, height);
    }

    /**
     * Set Home Page.
     */
    public String setHome(String prompt) {
        try {
            homePage = new TextInputDialog();
            homePage.setContentText(prompt);
            myHomePage = homePage.showAndWait();
            URL myHomePageURL = myBrowser.completeURL(myHomePage.get());
            myHomePageURL.openStream();
        }
        catch(Exception e){
            showError("Invalid Home Page Inputted");
            setHome(prompt);
        }
        myHomePageLink = myHomePage.get();
        return myHomePageLink;
    }

    /**
     * Display given URL.
     */
    public void showPage (String url) {
        String currentURL;
        if(startBrowser == true){
            currentURL = setHome("Set Home Page");
        }
        else{
            currentURL = url;
        }
        myBrowser.trackWebsiteVisits(currentURL);
        startBrowser = false;
        try {
            URL tmp = myBrowser.completeURL(currentURL);
            if (tmp != null) {
                // unfortunately, completeURL may not have returned a valid URL, so test it
                tmp.openStream();
                // if successful, remember this URL
            }
            myBrowser.updateHistory(tmp);
            update(myBrowser.getCurrentURL());
        }
        catch (Exception e) {
            showError(String.format("Could not load %s", currentURL));
        }
    }

    /**
     * Move to next URL.
     */
    public void next () {
        myBrowser.indexNext();
        update(myBrowser.getURLFromHistory());
    }

    /**
     * Move to previous URL.
     */
    public void back () {
        myBrowser.indexBack();
        update(myBrowser.getURLFromHistory());
    }

    /**
     * Add favorites.
     */
    public void favorite(){
        TextInputDialog favoriteName = new TextInputDialog();
        favoriteName.setContentText("Please input a name for your favorite site");
        favoriteName.showAndWait();
        String favoriteNameString = favoriteName.getEditor().getText();
        if(favoriteNameString.length() == 0 || (myFavoriteNames != null && myFavoriteNames.contains(favoriteNameString))){
            showError("Invalid name entered - Must enter a name, no repeat names");
            favorite();
        }
        else{
            myFavoriteNames.add(favoriteNameString);
            myFavoriteSites.put(favoriteNameString, myBrowser.getCurrentURL());
            myFavorites.getItems().add(favoriteNameString);
        }
    }

    /**
     * Go to current favorite.
     */
    public void setCurrentFavorite(){
        if(myFavorites.getValue() != null){
            selectedFavorite = myFavorites.getValue();
        }
        showPage(myFavoriteSites.get(selectedFavorite).toString());

    }

    /**
     * Add top sites.
     */
    public void topSites(){
        myVisitedSites = myBrowser.getMyVisitCount();
        int maxVisits = Collections.max(myVisitedSites.values());
        for(String s: myVisitedSites.keySet()){
            if(myVisitedSites.get(s) == maxVisits && !myTopSites.getItems().contains(s)){
                myTopSites.getItems().add(s);
            }
            if(myVisitedSites.get(s) != maxVisits && myTopSites.getItems().contains(s)){
                myTopSites.getItems().remove(s);
            }
        }
    }

    /**
     * Go to top sites
     */
    public void setTopSites(){
        if(myTopSites.getValue() != null){
            selectedTopSite = myTopSites.getValue();
        }
        showPage(selectedTopSite);
    }

    /**
     * Add and go to sites in the history
     */
    private void setHistory() {
        URL lastURL = myBrowser.getURLFromHistory();
        MenuItem m = new MenuItem(lastURL.toString());
        myRecentSites.getItems().add(m);
        m.setOnAction(event -> showPage(m.getText()));
    }

    /**
     * update URL.
     */
    public void update (URL url) {
        String urlText = url.toString();
        myPage.getEngine().load(urlText);
        myURLDisplay.setText(urlText);
    }

    /**
     * Show URL
     */
    private void showStatus (String message) {
        myStatus.setText(message);
    }

    /**
     * Make top panel JavaFX components
     */
    private Node makeTopPanel () {
        HBox result = new HBox();
        // create buttons, with their associated actions
        Button backButton = makeButton("Back", event -> back());
        result.getChildren().add(backButton);
        Button nextButton = makeButton("Next", event -> next());
        result.getChildren().add(nextButton);

        // if user presses button or enter in text field, load/show the URL
        EventHandler<ActionEvent> showHandler = event -> showPage(myURLDisplay.getText());
        result.getChildren().add(makeButton("Go", showHandler));
        myURLDisplay = makeInputField(40, showHandler);
        result.getChildren().add(myURLDisplay);

        Button homeButton = makeButton("Home", event -> showPage(myHomePageLink));
        result.getChildren().add(homeButton);

        Button refreshButton = makeButton("Refresh", event -> showPage(myBrowser.getCurrentURL().toString()));
        result.getChildren().add(refreshButton);

        return result;
    }

    /**
     * Make right panel JavaFX components.
     */
    private Node makeRightPanel(){
        VBox side = new VBox();
        Button favoriteButton = makeButton("Add to Favorites:", event -> favorite());
        side.getChildren().add(favoriteButton);
        ChoiceBox<String> myCurrentFavorites = makeFavoritesBox(event -> setCurrentFavorite());
        side.getChildren().add(myCurrentFavorites);

        Button topSites = makeButton("Refresh Top Sites:", event -> topSites());
        side.getChildren().add(topSites);
        ChoiceBox<String> myTopSites = makeTopSites(event -> setTopSites());
        side.getChildren().add(myTopSites);

       SplitMenuButton history = makeSplitMenuButton(event -> setHistory());
       side.getChildren().add(history);
        return side;
    }
    /**
     * Display error message pop-up
     */
    private void showError (String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Browser Error");
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Make display for "would be" URL
     */
    private Node makeInformationPanel () {
        // BLANK must be non-empty or status label will not be displayed in GUI
        myStatus = new Label(BLANK);
        return myStatus;
    }

    /**
     * Display HTML display.
     */
    private Node makePageDisplay () {
        myPage = new WebView();
        // catch "browsing" events within web page
        myPage.getEngine().getLoadWorker().stateProperty().addListener(new LinkListener());
        return myPage;
    }

    /**
     * Create button.
     */
    private Button makeButton (String label, EventHandler<ActionEvent> handler) {
        Button result = new Button();
        result.setText(label);
        result.setOnAction(handler);
        return result;
    }

    /**
     * Create favorites dropdown.
     */
    private ChoiceBox<String> makeFavoritesBox(EventHandler<ActionEvent> handler){
        myFavorites = new ChoiceBox<>();
        myFavorites.setOnAction(handler);
        return myFavorites;
    }

    /**
     * Create top sites dropdown.
     */
    private ChoiceBox<String> makeTopSites(EventHandler<ActionEvent> handler){
        myTopSites = new ChoiceBox<>();
        myTopSites.setOnAction(handler);
        return myTopSites;
    }

    /**
     * Create history split menu button.
     */
    private SplitMenuButton makeSplitMenuButton(EventHandler<ActionEvent> handler){
        myRecentSites = new SplitMenuButton();
        myRecentSites.setText("History");
        myRecentSites.setOnAction(handler);
        return myRecentSites;
    }

    /**
     * Create input field for URL
     */
    private TextField makeInputField (int width, EventHandler<ActionEvent> handler) {
        TextField result = new TextField();
        result.setPrefColumnCount(width);
        result.setOnAction(handler);
        return result;
    }
    // Inner class to deal with link-clicks and mouse-overs Mostly taken from
    //   http://blogs.kiyut.com/tonny/2013/07/30/javafx-webview-addhyperlinklistener/
    public class LinkListener implements ChangeListener<Worker.State> {
        public static final String ANCHOR = "a";
        public static final String HTML_LINK = "href";
        public static final String EVENT_CLICK = "click";
        public static final String EVENT_MOUSEOVER = "mouseover";
        public static final String EVENT_MOUSEOUT = "mouseout";

        @Override
        public void changed (ObservableValue<? extends Worker.State> ov, Worker.State oldState, Worker.State newState) {
            if (newState == Worker.State.SUCCEEDED) {
                EventListener listener = event -> handleMouse(event);
                Document doc = myPage.getEngine().getDocument();
                NodeList nodes = doc.getElementsByTagName(ANCHOR);
                for (int k = 0; k < nodes.getLength(); k+=1) {
                    EventTarget node = (EventTarget)nodes.item(k);
                    node.addEventListener(EVENT_CLICK, listener, false);
                    node.addEventListener(EVENT_MOUSEOVER, listener, false);
                    node.addEventListener(EVENT_MOUSEOUT, listener, false);
                }
            }
        }

        // Give user feedback as expected by modern browsers
        private void handleMouse (Event event) {
            final String href = ((Element)event.getTarget()).getAttribute(HTML_LINK);
            if (href != null) {
                switch (event.getType()) {
                    case EVENT_CLICK -> showPage(href);
                    case EVENT_MOUSEOVER -> showStatus(href);
                    case EVENT_MOUSEOUT -> showStatus(BLANK);
                }
            }
        }
    }
}
