package browser;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;




/**
 * A class used to display the viewer for a simple HTML browser.
 *
 * See this tutorial for help on how to use all variety of components:
 *   http://download.oracle.com/otndocs/products/javafx/2/samples/Ensemble/
 *
 * @author Owen Astrachan
 * @author Marcin Dobosz
 * @author Yuzhang Han
 * @author Edwin Ward
 * @author Robert C. Duvall
 */
public class NanoBrowser {
    // data
    private URL myCurrentURL;
    private int myCurrentIndex;
    private List<URL> myHistory;
    private HashMap<String, Integer> myVisitCount = new HashMap<>();



    /**
     * Create a web browser with prompts in the given language with initially empty state.
     */
    public NanoBrowser () {
        myCurrentURL = null;
        myCurrentIndex = -1;
        myHistory = new ArrayList<>();
    }
    /**
     * Update url visit history
     */
    public void updateHistory(URL tmp){
        myCurrentURL = tmp;
        if (hasNext()) {
            myHistory = myHistory.subList(0, myCurrentIndex + 1);
        }
        myHistory.add(myCurrentURL);
        System.out.println(myCurrentURL);
        myCurrentIndex += 1;
    }

    /**
     * Get current URL user is at
     */
    public URL getCurrentURL(){
        return myCurrentURL;
    }

    /**
     * Shift index forward or back dependent on next/back call
     */
    public void indexNext(){ myCurrentIndex++;}
    public void indexBack(){myCurrentIndex --;}

    /**
     * Keep track of how many times each website is visited
     */
    public void trackWebsiteVisits(String siteJustVisited){
        if(myVisitCount.containsKey(siteJustVisited)){
            Integer count = myVisitCount.get(siteJustVisited);
            myVisitCount.put(siteJustVisited, count + 1);
        }
        else if(!myVisitCount.containsKey(siteJustVisited)){
            myVisitCount.put(siteJustVisited, 1);
        }
    }

    /**
     * Get how many times a website has been visited
     */
    public HashMap<String, Integer> getMyVisitCount() {
        return myVisitCount;
    }

    /**
     * Get a URL that has already been visited earlier
     */
    public URL getURLFromHistory(){
        return myHistory.get(myCurrentIndex);
    }



    /**
     * Check if a next URL is available
     */
    private boolean hasNext () {
        return myCurrentIndex < (myHistory.size() - 1);
    }



    /**
     * Deal with incomplete URLs
     */
    public URL completeURL (String possible) {
        final String PROTOCOL_PREFIX = "http://";
        try {
            // try it as is
            return new URL(possible);
        }
        catch (MalformedURLException e) {
            try {
                // e.g., let user leave off initial protocol
                return new URL(PROTOCOL_PREFIX + possible);
            }
            catch (MalformedURLException ee) {
                try {
                    // try it as a relative link
                    // FIXME: need to generalize this :(
                    return new URL(myCurrentURL.toString() + "/" + possible);
                }
                catch (MalformedURLException eee) {
                    // FIXME: not a good way to handle an error!
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

}
