package browser;

import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class NanoBrowserTest {
    @Test
    void testUpdateHistoryError(){
        NanoBrowser b = new NanoBrowser();
        try{
            b.updateHistory(new URL("google"));
            fail("This URL should not work");
        }
        catch(MalformedURLException e){
            assert(true);
        }
    }
    @Test
    void testUpdateHistoryValid(){
        NanoBrowser b = new NanoBrowser();
        try{
            b.updateHistory(new URL("http://google.com"));
            assert(true);
        }
        catch(MalformedURLException e){
            fail("this should pass");
        }
    }

    @Test
     void getCurrentURLNull(){
        NanoBrowser b = new NanoBrowser();
        assertNull(b.getCurrentURL());
    }

    @Test
    void getCurrentURLNotNull(){
        NanoBrowser b = new NanoBrowser();
        URL currentURL = b.completeURL("google.com");
        b.updateHistory(currentURL);
        assertEquals(currentURL, b.getCurrentURL());
    }

    @Test
    void trackWebsiteVisitsInitial(){
        NanoBrowser b = new NanoBrowser();
        HashMap<String, Integer> visited = new HashMap<>();
        visited.put("google.com", 1);
        b.trackWebsiteVisits("google.com");
        assertEquals(visited, b.getMyVisitCount());
    }

    @Test
    void trackWebsiteVisitsNotInitial(){
        NanoBrowser b = new NanoBrowser();
        HashMap<String, Integer> visited = new HashMap<>();
        visited.put("google.com", 2);
        b.trackWebsiteVisits("google.com");
        b.trackWebsiteVisits("google.com");
        assertEquals(visited, b.getMyVisitCount());
    }

    @Test
    void backCheckNull(){
        NanoBrowser b = new NanoBrowser();
        try {
            b.indexBack();
            b.getURLFromHistory();
            fail("Should not be able to go back");
        }
        catch(IndexOutOfBoundsException e){
            assert(true);
        }
    }

    @Test
    void nextCheckNull(){
        NanoBrowser b = new NanoBrowser();
        try {
            b.indexNext();
            b.getURLFromHistory();
            fail("Should not be able to go next");
        }
        catch(IndexOutOfBoundsException e){
            assert(true);
        }
    }

    @Test
    void backCheckNotNull(){
        NanoBrowser b = new NanoBrowser();
        List<URL> history = new ArrayList<>();
        history.add(b.completeURL("google.com"));
        history.add(b.completeURL("gmail.com"));
        for(int k = 0; k < 2; k++){
            b.updateHistory(history.get(k));
        }
        b.indexBack();
        assertEquals(history.get(0), b.getURLFromHistory());

    }
    @Test
    void nextCheckNotNull(){
        NanoBrowser b = new NanoBrowser();
        List<URL> history = new ArrayList<>();
        history.add(b.completeURL("google.com"));
        history.add(b.completeURL("gmail.com"));
        for(int k = 0; k < 2; k++){
            b.updateHistory(history.get(k));
        }
        b.indexBack();
        b.indexNext();
        assertEquals(history.get(1), b.getURLFromHistory());
    }

}
