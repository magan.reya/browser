Browser
====

This project implements a web browser.

Name: Reya Magan

### Timeline

Start Date: 9/17

Finish Date: 9/19

Hours Spent: 18


### Tutorial and other Resources Used
http://tutorials.jenkov.com/javafx/splitmenubutton.html - I used this to understand how to set up the Split Menu Button

https://www.w3schools.com/java/java_try_catch.asp - refresher on try/catch blocks

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/ChoiceBox.html#getValue-- -Choice Box instructions

http://junit.sourceforge.net/javadoc/org/junit/Assert.html - list of assertions for test class

https://www.geeksforgeeks.org/javafx-textinputdialog/ - text input dialog exceptions

### Resource Attributions
Besides the resources above, no other resources were used.

### Running the Program

Main class: Main.java

Data files needed: n/a

Key/Mouse inputs: You will be prompted to enter the home page as well as a title for every favorites you add. In order to keep track of top sites, you will need to click the refresh top sites button. For the history feature, it needs to be clicked at every new website visit. If you want to go to a link from one of the dropdown menus, you just have to click it.

Known Bugs: The program can be a little slow, but it does eventually go to the right link. One bug I noticed in the provided lab code itself was that on certain websites with embedded links (for example Google searches), the program does not load a full link - rather it adds onto the already existing link and due to this it will go to this new link, but return an error about the link being invalid (the program then does not update its history). 
I tried fixing this bug, but I still need more practice understanding how the URL class works, so I have not been able to fix it just yet.


### Notes/Assumptions
The program works correctly, but sometimes cannot recognize embedded links (as explained above).
The tests have an 81% line coverage.

### Impressions
I really liked this project, I feel like I understood how to separate the JavaFX components from the Model components early on, so adding new features felt much more organized.

